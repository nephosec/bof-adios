### BOF-Adios

BOF Adios is based on the (awesome) work from Nerdworks Blogorama, which you can find here: https://blogorama.nerdworks.in/selfdeletingexecutables/

The logic is pretty straightforward:

* Duplicate a handle to the current process into a sacrificial process
* Replace a dummy value in the shellcode with that handle
* The remote thread takes the handle and gets the filename associated with the process
* The remote thread waits on the process to terminate
* The operator exits the beacon, killing the process
* The remote thread will zero, then delete the file associated with the (dead) process

NOTE!!!

1. Don't trust my shellcode, compile the Die project yourself *in realease* and extract with extract.py then add to the adios.h header
2. Don't use this if your beacon is being launched by something you don't want to / can't delete, e.g. powershell one-liner
3. Don't inject x64 into an x86 process, etc, use common sense


### Usage

adios x86|x64

NOTE!!!

Pay attention to sacrificial processes you may have defined in your malleable profile!