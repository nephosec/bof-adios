import pefile
import argparse
import logging
import os

def output_raw(args, shellcode):
    try:
        open(args.outfile, 'wb').write(shellcode)
    except Exception as ex:
        logging.fatal(ex)
        exit(0)
    
def output_c(args, shellcode):
    c_fmt_0 = "unsigned char SHELLCODE_DIE[] = {{\n\t{}\n}};"

    formatted_shellcode = []
    for i in range(0, len(shellcode), 16):
        formatted_shellcode.append(
            ','.join( [ str(int(x)) for x in shellcode[i:i+16]] )
        )

    formatted_output = c_fmt_0.format(
        ',\n\t'.join([x for x in formatted_shellcode])
    )
    try:
        open(args.outfile, 'w').write(formatted_output)
    except Exception as ex:
        logging.fatal(ex)
        exit(0)

SUPPORTED_FORMATS = {
    'Raw': output_raw,
    'C': output_c
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', action='store_true', default=False, dest='debug', help='Enable debug output')
    parser.add_argument('-l', action='store', default=None,  dest='shellcode_loader_file', required=True,  help='file containin shellcode to inject')
    parser.add_argument('-o', action='store', default=None, dest='outfile', required=True, help='output reflectively loading dll')
    parser.add_argument('-f', action='store', default=None, dest='format', help='Format of the output payload [Raw, C]')
    parser.add_argument('-s', action='store', default=None, dest='section', help='name of the section containing the shellcode')
    parser.add_argument('-t', action='store_true', default=False, dest='trim', help='trim null bytes at the end of a section')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    global SUPPORTED_FORMATS
    if args.format not in SUPPORTED_FORMATS.keys():
        logging.critical("Unsupported format {}".format(args.format))
        parser.print_help()
        exit(0)

    shellcode_loader_file = args.shellcode_loader_file
    if os.path.exists(args.outfile):
        try:
            logging.info("removing old file")
            os.remove(args.outfile)
        except Exception as ex:
            logging.info("couldn't delete old file, please manually delete {}".format(args.outfile))
            exit(0)

    if shellcode_loader_file is None:
        parser.print_help()
        exit(0)
    
    if not os.path.exists(shellcode_loader_file):
        logging.fatal("Error, shellcode file {} does not exist".format(shellcode_loader_file))
        exit(0)

    shellcode = None
    # get the shellcode from the .shlcode section of the shellcode file

    if args.section is not None:
        logging.debug("Processing shellcode file {}".format(shellcode_loader_file))
        shellcode_pe = pefile.PE(shellcode_loader_file)

        for section in shellcode_pe.sections:
            if args.section.encode() == section.Name.strip(b'\x00'):
                logging.debug("Found section with data: {}".format(args.section))
                shellcode = section.get_data()
                break
    else:
        logging.fatal("Error, shellcode section not specified!")
        exit(0)

    # append it to the target file
    if shellcode is None:
        logging.fatal("Shellcode file invalid, or does not exist")
        exit(0)

    logging.info("Loaded {} bytes from shellcode file {}".format(len(shellcode), shellcode_loader_file))
    pass

    if args.trim:
        shellcodecpy = shellcode[::-1]
        for i in range(0, len(shellcodecpy)):
            if shellcodecpy[i] != 0:
                logging.info("trimming {} null bytes...".format(i))
                shellcode = shellcode[:len(shellcode) - i]
                #logging.debug("C Array of shellcode : {}".format(
                #    ' ,'.join([hex(x) for x in shellcode])
                #))
                break
    
    SUPPORTED_FORMATS[args.format](args, shellcode)

    pass

if __name__ == "__main__":
    main()